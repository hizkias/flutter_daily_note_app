import 'package:daily_note/models/note.dart';
import 'package:daily_note/views/createNote.dart';
import 'package:daily_note/views/editNote.dart';
import 'package:daily_note/views/readNote.dart';
import 'package:flutter/material.dart';
import '../utils/uiHelper.dart';
import '../utils/databaseHelper.dart';

class NoteApp extends StatelessWidget {
  final DatabaseHelper dbHelper = DatabaseHelper();
  @override
  Widget build(BuildContext context) {
    return UiHelper.createMaterialApp(_NoteHome());
  }
}

class _NoteHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NoteHomeState();
  }
}

class _NoteHomeState extends State<_NoteHome> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  Future<List<Map<String, dynamic>>> _notesList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _notesList = DatabaseHelper().getAllNotes();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('My note'),
      ),
      body: FutureBuilder<List<Map<String, dynamic>>>(
        future: _notesList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length == 0) {
              return Center(
                child: Text('Notes have not been added'),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var item = snapshot.data[index];
                    return Dismissible(
                        key: Key(DateTime.now().toString()),
                        onDismissed: (direction){
                          DatabaseHelper().deleteNote(item['id']);
                          var snackbar = SnackBar(content: Text('Deleted a note!'),action: SnackBarAction(
                              label: 'Undo', onPressed: (){}),
                          );

                          _scaffoldKey.currentState.hideCurrentSnackBar();
                          _scaffoldKey.currentState.showSnackBar(snackbar);
                        },
                        child: ListTile(
                          leading: CircleAvatar(
                            child: Icon(Icons.note),
                          ),
                          title: Text('${item['title']}'),
                          subtitle: Text('${UiHelper.formatTime(item['date'].toString())}'),
                          trailing: IconButton(
                              icon: Icon(Icons.edit), onPressed: () async{
                            await Navigator.push(context, MaterialPageRoute(builder: (context){
                              return EditNote(item['id']);
                            }));
                            setState(() {
                              _notesList = DatabaseHelper().getAllNotes();
                            });
                          }),
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                              return ReadNote(item['id']);
                            }));
                          },
                        ));
                  });
            }
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Some error'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CreateNote();
          }));

          setState(() {
            _notesList = DatabaseHelper().getAllNotes();
          });
//        final snackbar = SnackBar(content: Text('Back from create window'),);
//        _scaffoldKey.currentState.showSnackBar(snackbar);
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

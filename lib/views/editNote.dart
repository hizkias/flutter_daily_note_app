import 'package:daily_note/models/note.dart';
import 'package:daily_note/utils/databaseHelper.dart';
import 'package:daily_note/utils/uiHelper.dart';
import 'package:daily_note/views/home.dart';
import 'package:flutter/material.dart';

class EditNote extends StatelessWidget{
  final int id;

  EditNote(this.id);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createMaterialApp(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            }
        ),
        title: Text('My note'),
      ),
      body: ListView(
        children: <Widget>[
          _EditNoteForm(id)
        ],
      ),
    ));
  }

}
class _EditNoteForm extends StatefulWidget{
  int id;

  _EditNoteForm(this.id);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditNoteFormState();
  }
}

class _EditNoteFormState extends State<_EditNoteForm>{

  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  TextEditingController _title = TextEditingController();
  TextEditingController _content = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<Map<String, dynamic>>(
      future: DatabaseHelper().getANote(widget.id),
      builder: (context, snapshot){
        if(snapshot.hasData){
          _title.text = snapshot.data['title'];
          _content.text = snapshot.data['content'];
          return Form(
            key: _formState,
            child: Container(
              padding: EdgeInsets.all(12),
              child: Column(
                children: <Widget>[
                  Text('Edit note', style: TextStyle(fontSize: 18),),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8),
                    child: TextFormField(
                      decoration: InputDecoration(
                          icon: Icon(Icons.title),
                          hintText: 'Title'
                      ),
                      controller: _title,
                      validator: (value){
                        if(value.isEmpty){
                          return 'Note title can not be empty';
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8),
                    child: TextFormField(
                      decoration: InputDecoration(
                          icon: Icon(Icons.description),
                          hintText: 'Content'
                      ),
                      controller: _content,
                      maxLines: 8,

                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8),
                    child: RaisedButton.icon(
                        onPressed: (){
                          if(_formState.currentState.validate()){
                            var snackbar = SnackBar(content: Text('Updating your note...'));
                            Scaffold.of(context).showSnackBar(snackbar);
                            var dbHelper = DatabaseHelper();
                            var result = dbHelper.editNote(widget.id,{
                              'title': _title.text,
                              'content': _content.text,
                            });
                            if(result != null){
                              snackbar = SnackBar(content: Text('Updated your note!'));
                              Scaffold.of(context).hideCurrentSnackBar();
                              Scaffold.of(context).showSnackBar(snackbar);
                            }
                          }
                        },
                        icon: Icon(Icons.update),
                        label: Text('Update')
                    ),
                  )
                ],
              ),
            ),
          );
        }
        else if(snapshot.hasError){
          return Center(
            child: Text('Error while getting data'),
          );
        }

        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

}
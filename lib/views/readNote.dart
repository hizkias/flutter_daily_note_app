import 'package:flutter/material.dart';
import '../utils/databaseHelper.dart';
import '../utils/uiHelper.dart';
import '../models/note.dart';

class ReadNote extends StatelessWidget{
  int id;
  ReadNote(this.id){}
  @override
  Widget build(BuildContext context) {

    return UiHelper.createMaterialApp(
      Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.pop(context);
              }),
          title: Text('My note'),
        ),
        body: FutureBuilder<Map<String, dynamic>>(
          future: DatabaseHelper().getANote(id),
          builder: (context, snapshot){
            if(snapshot.hasData){

              return ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(14),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(children: <Widget>[
                          Expanded(child: Text('${snapshot.data['title']}', style: TextStyle(fontSize: 18),)),
                          Text('${UiHelper.formatTime(snapshot.data['date'].toString())}')
                        ],),
                        Divider(),
                        Text('${snapshot.data['content'].toString().isEmpty ? "" : snapshot.data['content'] }')
                      ],
                    ),
                  )
                ],
              );
            }
            else if(snapshot.hasError){
              print(snapshot.error);
              return Center(
                child: Text('Error while loading data'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      )
    );
  }


}
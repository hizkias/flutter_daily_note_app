import 'package:daily_note/models/note.dart';
import 'package:daily_note/utils/databaseHelper.dart';
import 'package:daily_note/utils/uiHelper.dart';
import 'package:daily_note/views/home.dart';
import 'package:flutter/material.dart';

class CreateNote extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createMaterialApp(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            }
        ),
        title: Text('My note'),
      ),
      body: ListView(
        children: <Widget>[
          _CreateNoteForm()
        ],
      ),
    ));
  }

}
class _CreateNoteForm extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CreateNoteFormState();
  }
}

class _CreateNoteFormState extends State<_CreateNoteForm>{
  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  TextEditingController _title = TextEditingController();
  TextEditingController _content = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
      key: _formState,
      child: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            Text('Add note', style: TextStyle(fontSize: 18),),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: TextFormField(
                decoration: InputDecoration(
                    icon: Icon(Icons.title),
                    hintText: 'Title'
                ),
                controller: _title,
                validator: (value){
                  if(value.isEmpty){  
                    return 'Note title can not be empty';
                  }
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: TextFormField(
                decoration: InputDecoration(
                    icon: Icon(Icons.description),
                    hintText: 'Content'
                ),
                controller: _content,
                maxLines: 8,

              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: RaisedButton.icon(
                  onPressed: (){
                    if(_formState.currentState.validate()){
                      var snackbar = SnackBar(content: Text('Saving your note...'));
                      Scaffold.of(context).showSnackBar(snackbar);
                      var dbHelper = DatabaseHelper();
                      var note = Note.fromMap({
                        'title': _title.text,
                        'content': _content.text,
                        'date': DateTime.now().toString()
                      });

                      var result = dbHelper.addNote({
                        'title': _title.text,
                        'content': _content.text,
                        'date': DateTime.now().toString()
                      });
                      if(result != null){
                        snackbar = SnackBar(content: Text('Created your note!'));
                        Scaffold.of(context).hideCurrentSnackBar();
                        Scaffold.of(context).showSnackBar(snackbar);
                      }
                    }
                  },
                  icon: Icon(Icons.save),
                  label: Text('Save')
              ),
            )
          ],
        ),
      ),
    );
  }

}
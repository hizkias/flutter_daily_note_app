import 'package:daily_note/models/note.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;
  DatabaseHelper._createInstance();

  factory DatabaseHelper(){
    if(_databaseHelper == null){
      _databaseHelper = DatabaseHelper._createInstance();
    }
    return _databaseHelper;

  }
  Future<Database> get database async {

    if(_database == null){
      _database = await this._initializeDatabase();
    }
    return _database;

  }

  Future<Database> _initializeDatabase() async {

    var dbPath = await getApplicationDocumentsDirectory();
    var conString = dbPath.path + 'notes.db';

    var db = await openDatabase(conString, version: 1, onCreate: _createDatabase);

    return db;

  }

  void _createDatabase(Database db, int version) async{
    await db.execute('CREATE TABLE notes(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT, date TEXT)');
  }

  Future<int> addNote(Map<String, dynamic> note) async{

    var db = await this.database;
    var result = await db.insert('notes', note);
    return result;

  }

  Future<List<Map<String, dynamic>>> getAllNotes() async{

    var db = await this.database;
    var result = await db.query('notes', orderBy: 'id DESC');

    return result;
  }

  Future<Map<String, dynamic>> getANote(int id) async{

    var db = await this.database;
    var result = await db.query('notes', where: 'id = ?', whereArgs: [id]);

    return result.first;
  }

  Future<int> editNote(int id, Map<String, dynamic> data) async{

    var db = await this.database;
    var result = await db.update('notes', data, where: 'id = ?', whereArgs: [id]);
    return result;

  }

  Future<int> deleteNote(id) async{

    var db = await this.database;
    var result = await db.delete('notes', where: 'id = ?', whereArgs: [id]);
    return result;

  }
}

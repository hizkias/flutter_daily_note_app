import 'package:flutter/material.dart';

class UiHelper{

  static Widget createMaterialApp(Widget scaffold){
    return MaterialApp(
        theme: ThemeData(
            primaryColor: Colors.purple,
            accentColor: Colors.purpleAccent
        ),
        home: scaffold
    );
  }

  static String formatTime(String dateTime){
    var inputTime = DateTime.parse(dateTime);
    var currentTime = DateTime.now();
    var diff = currentTime.difference(inputTime);

    if(diff.inDays == 1 ){
      return 'Yesterday ${inputTime.hour}:${inputTime.minute}';
    }
    else if(diff.inDays == 0){
      if(diff.inHours == 0){
        return '${diff.inMinutes}.mins ago';
      }
      else if(diff.inHours == 1){
        return '${diff.inHours}.hr ago';
      }
      else {
        return '${diff.inHours}.hrs ago';
      }
    }

    else {
      return '${inputTime.day}/${inputTime.month}/${inputTime.year}';
    }

  }
}
class Note {
  int id;
  String title;
  String content;
  String date;

  Note(this.title, this.content, this.date, [this.id]);

  Note.fromMap(Map<String, dynamic> noteMap) {
    Note(noteMap['title'], noteMap['content'], noteMap['date'], noteMap['id']);
  }

  toMap() {
    Map<String, dynamic> noteMap = Map<String, dynamic>();

    noteMap['id'] = this.id;
    noteMap['title'] = this.title;
    noteMap['content'] = this.content;
    noteMap['date'] = this.date;

    return noteMap;
  }
}
